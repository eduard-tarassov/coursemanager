import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiHttpUrls {
  private readonly BASE_URL = window["courseManagerApiBaseURL"];
  public readonly coursesUrl = `${this.BASE_URL}/courses`;
  public readonly modulesUrl = `${this.BASE_URL}/modules`;

}
