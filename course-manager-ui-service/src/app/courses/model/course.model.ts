export class Course {
  private _id: number;
  private _description: string;
  private _name: string;
  private _rating: number;
  private _ratingCounter: number;
  private _category: string;
  private _modules: number[];

  get id(): number {
    return this._id;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get rating(): number {
    return this._rating;
  }

  set rating(value: number) {
    this._rating = value;
  }

  get ratingCounter(): number {
    return this._ratingCounter;
  }

  set ratingCounter(value: number) {
    this._ratingCounter = value;
  }

  get category(): string {
    return this._category;
  }

  set category(value: string) {
    this._category = value;
  }

  get modules(): number[] {
    return this._modules;
  }

  set modules(value: number[]) {
    this._modules = value;
  }
}
