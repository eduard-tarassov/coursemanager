import { Component, OnInit } from '@angular/core';
import { Course } from './model/course.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
  courses: Course[] = [];

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.courses = this.route.snapshot.data.coursesData; // get data from resolver
  }
}
