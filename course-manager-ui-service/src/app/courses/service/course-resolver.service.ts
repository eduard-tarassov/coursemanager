import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { take, map } from 'rxjs/operators';

import { Observable } from 'rxjs';
import { CourseService } from './course.service';

@Injectable({
  providedIn: 'root'
})
export class CourseResolverService implements Resolve <Observable<any>>{

  constructor(private coursesService: CourseService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    return this.coursesService.getAll().pipe(
      take(1),
      map(modules => modules)
    )
  }
}
