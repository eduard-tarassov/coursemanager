import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Course } from '../model/course.model';
import { map } from 'rxjs/operators';
import { ApiHttpUrls } from '../../shared/constants/api-http-urls';

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  apiHttpUrls: ApiHttpUrls = new ApiHttpUrls();

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<Course[]> {
    return this.http.get(this.apiHttpUrls.coursesUrl) as Observable<Course[]>;
  }

  getCategorisedCourses(category: string): Observable<Course[]> {
    return this.getAll().pipe(
      map(courses => courses.filter(course => course.category === category))
    );
  }
}
