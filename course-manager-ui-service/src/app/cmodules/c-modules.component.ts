import { Component, OnInit } from '@angular/core';
import { CModule } from './model/c-module.model';
import { ActivatedRoute, Router } from '@angular/router';
import { CourseService } from '../courses/service/course.service';
import { map, switchMap, tap } from 'rxjs/operators';
import { Course } from '../courses/model/course.model';
import { CModulesService } from './service/c-modules.service';

@Component({
  selector: 'app-modules',
  templateUrl: './c-modules.component.html',
  styleUrls: ['./c-modules.component.scss']
})
export class CModulesComponent implements OnInit {
  category;
  cmodules: CModule[] = [];
  filteredCModules: CModule[] = [];
  courses: Course[];
  selectedModuleIds: number[] = [];


  readonly categories: string[] = [
    'All Categories', 'TOP 10 Modules this Month', 'Introduction to Cyber Security',
    'Cybersecurity for Business', 'Cybersecurity Testing'];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private courseService: CourseService,
              private modulesService: CModulesService) {

    /**
     * Subscribing to route sending module IDs attached to particular course.
     * Then simply filtering all necessary modules.
     */
    route.params.subscribe(params => {
      this.selectedModuleIds = params.moduleIds;
      if (this.selectedModuleIds) {
        this.modulesService.getAll()
          .subscribe(newCmodules => {
            this.filteredCModules = newCmodules.filter((data: CModule) => this.selectedModuleIds.includes(data.id));
          });
      }
    });

    /**
     * Subscribing to route sending category selected by the user.
     * Then finding Courses with corresponding category.
     * filterCategorisedModules function is responsible for finding
     * modules which are attached to found courses.
     */
    route.queryParamMap.pipe(
      map(params => {
        this.category = params.get('category');
        this.filteredCModules = this.cmodules;
      }),
      switchMap(category =>
        courseService.getCategorisedCourses(this.category)
      ),
      tap(courses => this.filterCategorisedModules(courses)))
      .subscribe();

  }

  ngOnInit(): void {
    // Basic /modules route receiving preloaded modules array.
    if (this.route.snapshot.data.cmodulesData) {
      this.cmodules = this.route.snapshot.data.cmodulesData; // get data from resolver
      this.filteredCModules = this.cmodules; // get data from resolver
    }
  }

  /**
   *
   */
  filterCategorisedModules(courses: Course[]): void {
    if (this.category && this.category !== 'All Categories' && this.category !== 'TOP 10 Modules this Month') {
      let moduleIds: number[] = [];
      courses.forEach(course =>
        // new ES6 method of merging arrays without duplicates - O(n) complexity
        moduleIds = [...new Set([...moduleIds, ...course.modules])]
      );

      // this searches for all moduleIds and all modules to find corresponding modules.
      // this part has 0(n^2) complexity - there is probably a less consuming way.
      let selectedCModules: CModule[] = [];
      moduleIds.forEach(id => {
          this.cmodules.forEach(module => {
            if (module.id === id) {
              selectedCModules.push(module);
            }
          });
        }
      );
      this.filteredCModules = selectedCModules;
    } else if (this.category === 'TOP 10 Modules this Month') {
      this.filterTopMonthlyModules();
    }
  }

  /**
   * Picking only top 10 Monthly modules if
   * 'TOP 10 Modules this Month' category is selected.
   */
  filterTopMonthlyModules(): void {
    this.filteredCModules.sort((moduleA, moduleB) =>
      moduleA.watchCounter > moduleB.watchCounter ? -1 : 1);
    this.filteredCModules = this.filteredCModules.slice(0, 10);
  }

  /**
   * Incrementing watch counter for particular user on a user click.
   * Then post via http request resulting counter to update in DB.
   * @param cmoduleIndex
   */
  incrementWatchCounter(cmoduleIndex: number): void {
    this.filteredCModules[cmoduleIndex].watchCounter = this.filteredCModules[cmoduleIndex].watchCounter ?
      this.filteredCModules[cmoduleIndex].watchCounter + 1 : 1;
    // send HTTP to update cmodule watchCounter in MongoDB
    this.modulesService.updateCModule(this.filteredCModules[cmoduleIndex]).subscribe();
  }

  /**
   * Renavigate to show modules for a certain category.
   */
  goCategoryModules(category: string): void {
    this.router.navigate(['/modules'], {queryParams: {category}});
  }
}
