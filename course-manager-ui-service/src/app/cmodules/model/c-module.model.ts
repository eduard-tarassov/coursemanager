export class CModule {
  private _id: number;
  private _name: string;
  private _description: string;
  private _type: string[];
  private _difficulty: string;
  private _courses: number[];
  private _watchCounter: number;

  get id(): number {
    return this._id;
  }

  get name(): string {
    return this._name;
  }

  get description(): string {
    return this._description;
  }

  get type(): string[] {
    return this._type;
  }

  get difficulty(): string {
    return this._difficulty;
  }

  get courses(): number[] {
    return this._courses;
  }

  get watchCounter(): number {
    return this._watchCounter;
  }

  set watchCounter(value: number) {
    this._watchCounter = value;
  }
}
