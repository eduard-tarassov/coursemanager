import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CModulesComponent } from './c-modules.component';

describe('ModulesComponent', () => {
  let component: CModulesComponent;
  let fixture: ComponentFixture<CModulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CModulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CModulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
