import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CModule } from '../model/c-module.model';
import { Observable, throwError } from 'rxjs';
import { catchError, filter, map } from 'rxjs/operators';
import { LocalErrorHandler } from '../../shared/util/local-error-handler';
import { ApiHttpUrls } from '../../shared/constants/api-http-urls';

@Injectable({
  providedIn: 'root'
})
export class CModulesService {
  apiHttpUrls: ApiHttpUrls = new ApiHttpUrls();
  constructor(private http: HttpClient) {
  }

  getAll(): Observable<CModule[]> {
    return this.http.get<CModule[]>(this.apiHttpUrls.modulesUrl);
  }

  // getDrilldownModules(selectedModuleIds: number[]): Observable<CModule[]> {
  //   // return this.http.get<CModule[]>(this.rootUrl + this.modulesUrl).pipe(map(cmodules =>
  //   //     filter((data: CModule) => selectedModuleIds.includes(data.id))
  //   //   ));
  //   return this.http.get<CModule[]>(this.rootUrl + this.modulesUrl);
  // }

  updateCModule(module: CModule): Observable<CModule> {
    console.log('updateCModule');

    console.log(JSON.stringify(module));

    return this.http.post<CModule>(this.apiHttpUrls.modulesUrl, module);
    // .pipe(
    //   catchError(LocalErrorHandler.handleError<CModule>('updateCModule', module))
    // );
    // .pipe(
    //   catchError(this.handleError('addHero', hero))
    // );
  }


}
