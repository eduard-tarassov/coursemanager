import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { take, map } from 'rxjs/operators';

import { Observable } from 'rxjs';
import { CModulesService } from './c-modules.service';

@Injectable({
  providedIn: 'root'
})
export class CModulesResolverService implements Resolve <Observable<any>> {

  constructor(private cmodulesService: CModulesService) {
  }

  /**
   * Resolver to load data before the /modules route is loaded.
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.cmodulesService.getAll().pipe(
      take(1),
      map(modules => modules)
    );
  }
}
