import { TestBed } from '@angular/core/testing';

import { CModulesService } from './c-modules.service';

describe('ModulesService', () => {
  let service: CModulesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CModulesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
