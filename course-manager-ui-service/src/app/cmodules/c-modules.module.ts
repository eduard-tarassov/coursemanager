import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CModulesComponent } from './c-modules.component';
import { TruncatePipe } from '../shared/util/truncate.pipe';

@NgModule({
  declarations: [
    CModulesComponent,
    TruncatePipe
  ],
  exports: [
    CModulesComponent
  ],
  imports: [
    CommonModule
  ]
})
export class CModulesModule {
}
