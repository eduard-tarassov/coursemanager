import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CoursesComponent} from './courses/courses.component';
import {CModulesComponent} from './cmodules/c-modules.component';
import {LoginComponent} from './login/login.component';
import {CModulesResolverService} from './cmodules/service/c-modules-resolver.service';
import {CourseResolverService} from './courses/service/course-resolver.service';

const routes: Routes = [
  {path: '', component: CoursesComponent,
    resolve: {
      coursesData: CourseResolverService
    }},
  {path: 'courses', component: CoursesComponent,
    resolve: {
      coursesData: CourseResolverService
    }},
  {path: 'modules', component: CModulesComponent,
    resolve: {
      cmodulesData: CModulesResolverService
    }},
  {path: 'login', component: LoginComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
