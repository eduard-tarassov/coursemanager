import { Component, OnInit } from '@angular/core';

/**
 * Custom navbar via bootstrap.
 */
@Component({
  selector: 'app-b-navbar',
  templateUrl: './b-navbar.component.html',
  styleUrls: ['./b-navbar.component.scss']
})
export class BNavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
