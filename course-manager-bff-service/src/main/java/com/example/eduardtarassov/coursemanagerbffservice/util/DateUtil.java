package com.example.eduardtarassov.coursemanagerbffservice.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    private static SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");


    public static Date today() throws ParseException {
        return formatter.parse(formatter.format(new Date()));
    }


    public static String todayStr() throws ParseException {
        return formatter.format(today());
    }

    public static Date stringToDate(String dateText) throws ParseException {
        return formatter.parse(dateText);
    }

    public static String formattedDate(Date date) throws ParseException {
        return date != null ? formatter.format(date) : todayStr();
    }

//    private static LocalDate dateToLocalDate(Date date) {
//        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//    }

    private static String dateToMonthValue(Date date) {
        return new SimpleDateFormat("MM").format(date);
    }

    private static String dateToYearValue(Date date) {
        return new SimpleDateFormat("YYYY").format(date);
    }

    /**
     * Check that both Dates have identical year and month.
     * @param date1
     * @param date2
     * @return boolean
     */
    public static boolean isMonthsAndYearsEqual(Date date1, Date date2) {
        return dateToMonthValue(date1).equals(dateToMonthValue(date2)) && dateToYearValue(date1).equals(dateToYearValue(date2));
    }
}