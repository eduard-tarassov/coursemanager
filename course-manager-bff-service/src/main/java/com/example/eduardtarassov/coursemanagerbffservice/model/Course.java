package com.example.eduardtarassov.coursemanagerbffservice.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "course")
public class Course {
    @Id
    private int _id;
    @Field(name = "Name")
    private String name;
    @Field(name = "Description")
    private String description;
    @Field(name = "Rating")
    private float rating;
    @Field(name = "Rating_counter")
    private int ratingCounter;
    @Field(name = "Category")
    private String category;
    @Field(name = "Modules")
    private int[] modules;

    public Course() {
    }

    public Course(int _id, String name, String description,
                  float rating, int ratingCounter, String category, int[] modules) {
        this._id = _id;
        this.name = name;
        this.rating = rating;
        this.ratingCounter = ratingCounter;
        this.category = category;
        this.modules = modules;
    }

    public int get_id() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getRatingCounter() {
        return ratingCounter;
    }

    public void setRatingCounter(int ratingCounter) {
        this.ratingCounter = ratingCounter;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int[] getModules() {
        return modules;
    }

    public void setModules(int[] modules) {
        this.modules = modules;
    }
}