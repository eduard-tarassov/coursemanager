package com.example.eduardtarassov.coursemanagerbffservice.repository;

import com.example.eduardtarassov.coursemanagerbffservice.model.CModule;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface ModuleRepository extends MongoRepository<CModule, String> {
    CModule findBy_id(int _id);

}