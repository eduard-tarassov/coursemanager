package com.example.eduardtarassov.coursemanagerbffservice.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "util")
public class UtilModel {
    @Id
    private int _id;
    @Field(name = "LastVisitorDate")
    private String lastVisitorDate;

    public UtilModel(int _id, String lastVisitorDate) {
        this._id = _id;
        this.lastVisitorDate = lastVisitorDate;
    }

    public int get_id() {
        return _id;
    }

    public String getLastVisitorDate() {
       return lastVisitorDate;

    }

    public void setLastVisitorDate(String lastVisitorDate) {
        this.lastVisitorDate = lastVisitorDate;
    }
}
