package com.example.eduardtarassov.coursemanagerbffservice.repository;

import com.example.eduardtarassov.coursemanagerbffservice.model.UtilModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UtilRepository extends MongoRepository<UtilModel, String> {
    UtilModel findBy_id(int _id);
}
