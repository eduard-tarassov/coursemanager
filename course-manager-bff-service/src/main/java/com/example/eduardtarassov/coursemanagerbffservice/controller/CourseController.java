package com.example.eduardtarassov.coursemanagerbffservice.controller;

import com.example.eduardtarassov.coursemanagerbffservice.model.Course;
import com.example.eduardtarassov.coursemanagerbffservice.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class CourseController {
    @Autowired
    private CourseRepository repository;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/courses")
    public List<Course> getAllCourses() {
        try {
            return repository.findAll();
        } catch (Exception exc) {
            System.out.println("Exception: " + exc.getMessage());
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Courses Not Found", exc);
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/courses/{id}")
    public Course getCourseById(@PathVariable("id") int id) {
        return repository.findBy_id(id);
    }
}