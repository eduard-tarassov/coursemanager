package com.example.eduardtarassov.coursemanagerbffservice.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Arrays;

@Document(collection = "module")
public class CModule {

    @Id
    private int _id;
    @Field(name = "Name")
    private String name;
    @Field(name = "Description")
    private String description;
    @Field(name = "Type")
    private String[] type;
    @Field(name = "Difficulty")
    private String difficulty;
    @Field(name = "Courses")
    private Integer[] courses;
    @Field(name = "WatchCounter")
    private int watchCounter;

    public CModule() {
    }

    public CModule(int _id, String name, String description,
                   String[] type, String difficulty,
                   Integer[] courses, int watchCounter) {
        this._id = _id;
        this.name = name;
        this.type = type;
        this.difficulty = difficulty;
        this.courses = courses;
        this.watchCounter = watchCounter;
    }


    public int getId() {
        return _id;
    }

    public void setId(int id) {
        this._id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getType() {
        return type;
    }

    public void setType(String[] type) {
        this.type = type;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public Integer[] getCourses() {
        return courses;
    }

    public void setCourses(Integer[] courses) {
        this.courses = courses;
    }

    public int getWatchCounter() {
        return watchCounter;
    }

    public void setWatchCounter(int watchCounter) {
        this.watchCounter = watchCounter;
    }

    @Override
    public String toString() {
        return "Module{" +
                "_id='" + _id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", type=" + Arrays.toString(type) +
                ", difficulty='" + difficulty + '\'' +
                ", courses=" + Arrays.toString(courses) +
                '}';
    }
}