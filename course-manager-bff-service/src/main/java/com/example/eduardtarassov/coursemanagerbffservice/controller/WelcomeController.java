package com.example.eduardtarassov.coursemanagerbffservice.controller;


import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class WelcomeController {

    @RequestMapping("/")
    public String index() {
        return "Greetings Spring Boot!";
    }

}