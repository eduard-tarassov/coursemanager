package com.example.eduardtarassov.coursemanagerbffservice.repository;

import com.example.eduardtarassov.coursemanagerbffservice.model.Course;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CourseRepository extends MongoRepository<Course, String> {
    Course findBy_id(int _id);
}