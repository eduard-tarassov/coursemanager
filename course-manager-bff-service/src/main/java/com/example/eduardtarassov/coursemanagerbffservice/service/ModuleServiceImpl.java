package com.example.eduardtarassov.coursemanagerbffservice.service;

import com.example.eduardtarassov.coursemanagerbffservice.model.CModule;
import com.example.eduardtarassov.coursemanagerbffservice.repository.ModuleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ModuleServiceImpl implements ModuleService {
    @Autowired
    private ModuleRepository repository;

    @Override
    public CModule findModuleById(int id) {
        return repository.findBy_id(id);
    }

    @Override
    public List<CModule> findAll() {
        return repository.findAll();
    }

    @Override
    public List<CModule> updateAll(List<CModule> cmodules) {
        return repository.saveAll(cmodules);
    }

    @Override
    public CModule update(CModule cmodule) {
        return repository.save(cmodule);
    }
}
