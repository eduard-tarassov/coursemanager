package com.example.eduardtarassov.coursemanagerbffservice.service;

import com.example.eduardtarassov.coursemanagerbffservice.model.CModule;

import java.util.List;

public interface ModuleService {

    public CModule findModuleById(int id);

    public List<CModule> updateAll(List<CModule> cmodules);

    public  List<CModule> findAll();

    public  CModule update(CModule cmodule);
}
