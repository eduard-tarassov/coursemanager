package com.example.eduardtarassov.coursemanagerbffservice.controller;

import com.example.eduardtarassov.coursemanagerbffservice.model.CModule;
import com.example.eduardtarassov.coursemanagerbffservice.model.UtilModel;
import com.example.eduardtarassov.coursemanagerbffservice.repository.UtilRepository;
import com.example.eduardtarassov.coursemanagerbffservice.service.ModuleService;
import com.example.eduardtarassov.coursemanagerbffservice.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@RestController
public class ModuleController {
    @Autowired
    private ModuleService moduleService;
    @Autowired
    private UtilRepository utilRepository;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/modules")
    public List<CModule> getAllModules() throws ParseException {
        List<CModule> foundModules = moduleService.findAll();

        UtilModel utilModel = utilRepository.findBy_id(1);

        Date lastVisitedDate = DateUtil.stringToDate(utilModel.getLastVisitorDate());
        Date currentDate = DateUtil.today();
        System.out.println(currentDate);

        if (lastVisitedDate.compareTo(currentDate) != 0) {
            if (!DateUtil.isMonthsAndYearsEqual(lastVisitedDate, currentDate)) {
                // update all modules with counter = 0
                // deep copy of foundModules
                List<CModule> copiedModules = List.copyOf(foundModules);

                // Update all counters to 0 value
                copiedModules.forEach((u) -> u.setWatchCounter(0));

                moduleService.updateAll(copiedModules);
            }
            // Update date in DB to current Date
            utilModel.setLastVisitorDate(DateUtil.todayStr());
            utilRepository.save(utilModel);
        }
        return foundModules;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/modules/{id}")
    public CModule getModuleById(@PathVariable("id") int id) {
        return moduleService.findModuleById(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(value = "/modules")
    public void updateModule(@RequestBody CModule cModule) {
        System.out.println("CModule Name: " + cModule.getName());
        System.out.println("CModule Counter: " + cModule.getWatchCounter());
        CModule updatedCModule =  moduleService.update(cModule);
    }

}
