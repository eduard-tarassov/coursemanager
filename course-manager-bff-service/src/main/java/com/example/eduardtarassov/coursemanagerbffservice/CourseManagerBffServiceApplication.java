package com.example.eduardtarassov.coursemanagerbffservice;

import com.example.eduardtarassov.coursemanagerbffservice.repository.ModuleRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class CourseManagerBffServiceApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
            SpringApplication.run(CourseManagerBffServiceApplication.class, args);

    }
}